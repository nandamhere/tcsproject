//
//  ScoreModel.swift
//  TCSProject
//
//  Created by Vaibhav Nandam on 4/1/21.
//

import Foundation

struct SatScore: Codable {
    let schoolName: String
    let readingScore: String
    let mathScore: String
    let writingScore: String
}

extension SatScore {
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}

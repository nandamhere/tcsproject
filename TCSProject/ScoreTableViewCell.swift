//
//  ScoreTableViewCell.swift
//  TCSProject
//
//  Created by Vaibhav Nandam on 4/1/21.
//

import UIKit

class ScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var avgReadingLable: UILabel!
    @IBOutlet weak var avgMathLabel: UILabel!
    @IBOutlet weak var avgWritingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

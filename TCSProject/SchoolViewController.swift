//
//  ViewController.swift
//  TCSProject
//
//  Created by Vaibhav Nandam on 4/1/21.
//

import UIKit

class SchoolViewController: UITableViewController, SchoolInfo {
    
    
    func success(_ schools: [School]) {
        
        self.schools = schools
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func failed(_: String) {

    }
    
    lazy var viewModel = SchoolViewModel(viewController: self)
    
    var schools: [School] = []
    var schoolName: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel.fetchSchoolDetails()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell")
        cell?.textLabel?.text = schools[indexPath.row].schoolName
        return cell ?? UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.schoolName = self.schools[indexPath.row].schoolName
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "scoreSegue", sender: schoolName)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let scoreVC = segue.destination as? ScoreTableViewController else {
            return
            
        }
        scoreVC.schoolName = schoolName
    }
}


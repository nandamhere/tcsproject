//
//  SchoolModel.swift
//  TCSProject
//
//  Created by Vaibhav Nandam on 4/1/21.
//

import Foundation

struct School: Codable {
    let schoolName: String
}

extension School {
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
    }
}

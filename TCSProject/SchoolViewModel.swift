//
//  SchoolViewModel.swift
//  TCSProject
//
//  Created by Vaibhav Nandam on 4/1/21.
//

import Foundation


protocol SchoolInfo {
    func success(_ schools: [School])
    func failed(_: String)
}

struct SchoolViewModel {
    
    let viewController: SchoolViewController & SchoolInfo
    
    init(viewController: SchoolViewController) {
        self.viewController = viewController
    }
    
    let url = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    
    func fetchSchoolDetails() {
        if let finalUrl = URL(string: url) {
            let session = URLSession(configuration: .default).dataTask(with: finalUrl) { (data, urlResponse, error) in
                self.handleResponse(data, urlResponse, error)
            }
            session.resume()
        }
    }
    
    func handleResponse(_ data: Data?, _ urlResponse: URLResponse?, _ error: Error?) {
        let decoder = JSONDecoder()
        if let data = data {
            do {
                let decodedData = try decoder.decode([School].self, from: data)
                viewController.success(decodedData)
               
            } catch {
                print("failed to decode")
            }
        } else {
            viewController.failed("failed to fetch school data")
        }
    }
    
}

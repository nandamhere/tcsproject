//
//  ScoreViewModel.swift
//  TCSProject
//
//  Created by Vaibhav Nandam on 4/1/21.
//

import Foundation

protocol ScoreInfo {
    func success(_ scores: [SatScore])
    func failed(_: String)
}

struct ScoreViewModel {
    
    let viewController: ScoreTableViewController & ScoreInfo
    
    init(viewController: ScoreTableViewController) {
        self.viewController = viewController
    }
    
    let url = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    func fetchScore() {
        if let finalUrl = URL(string: url) {
            let session = URLSession(configuration: .default).dataTask(with: finalUrl) { (data, urlResponse, error) in
                self.handleResponse(data, urlResponse, error)
            }
            session.resume()
        }
    }
    
    func handleResponse(_ data: Data?, _ urlResponse: URLResponse?, _ error: Error?) {
        let decoder = JSONDecoder()
        if let data = data {
            do {
                let decodedData = try decoder.decode([SatScore].self, from: data)
                viewController.success(decodedData)
            } catch {
                print("failed to decode")
            }
        } else {
            viewController.failed("failed to fetch scores")
        }
    }
    
}

//
//  ScoreTableViewController.swift
//  TCSProject
//
//  Created by Vaibhav Nandam on 4/1/21.
//

import UIKit

class ScoreTableViewController: UITableViewController, ScoreInfo {
    
    
    lazy var viewModel = ScoreViewModel(viewController: self)
    var schoolName: String?
    var score: SatScore?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchScore()
    }
    
    func success(_ scores: [SatScore]) {
        self.score = scores.first(where: {$0.schoolName == schoolName?.uppercased()})
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func failed(_: String) {
        // Necessary action for failure
        print("failed to fetch scores")
    }
    

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCell", for: indexPath) as? ScoreTableViewCell
        cell?.schoolNameLabel.text = "SCHOOL: \(score?.schoolName ?? "Not Available")"
        cell?.avgReadingLable.text = "Avg Reading Score: \(score?.readingScore ?? "Not Available")"
        cell?.avgMathLabel.text = "Avg Math Score: \(score?.mathScore ?? "Not Available")"
        cell?.avgWritingLabel.text = "Avg Writing Score: \(score?.writingScore ?? "Not Available")"
        return cell ?? UITableViewCell()
    }

}
